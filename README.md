- Upload file API with MultipartFile, CrossOrigin
- Upload file UI with 
   + HTML (/resources/static/fileupload.html) 
   + HTML (/resources/static/recaptcha.html) 
   + JavaScript with Ajax (XMLHttpRequest, multipart/form-data)