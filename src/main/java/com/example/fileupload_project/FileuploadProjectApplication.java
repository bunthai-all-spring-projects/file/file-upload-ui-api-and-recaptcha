package com.example.fileupload_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
@RequestMapping("/file")
@Configuration
public class FileuploadProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileuploadProjectApplication.class, args);
	}

	@PostMapping // (consumes = "multipart/form-data") is optional to add
	@CrossOrigin(origins = {"http://localhost:4200", "http://localhost:9000"} )
	public Map<String, Object> uploadFile(
		MultipartFile file
	) throws IOException {

		System.out.println("fileName: " + file.getOriginalFilename());
		System.out.println("fileSize: " + file.getSize());

		return new HashMap<String, Object>(){{
			put("fileName", file.getOriginalFilename());
			put("fileSize", file.getSize());
		}};

	}
}
